#!/bin/bash
 
# Find the environment that we're in
if [ ${PROMISE_GIT_USER:+x} ]	# Test whether variable is set and not empty
then
	environment=$PROMISE_GIT_USER
	# XXX: currently, we don't actually have git on any of our servers.
	# Nor do we have a stage server. So for now, treat the old FTP dev server as
	# corresponding to the stage branch, and the live server as corresponding to the
	# master branch.

	# We need to check which branch we're on. If we're on the stage branch, environment
	# is dev (confusing, I know, bear with me, it's only temporary), and if we're on the
	# branch, then the environment is master
	if [ -f .branch.tmp ]
	then
		branch="$(cat .branch.tmp)"
		rm .branch.tmp
		case $branch in
			'stage' )
				environment='stage'
				;;
			'master' )
				environment='master'
				;;
		esac
	fi
else
	hostname=`hostname -f`
	if [ $hostname = 'server.promisedev' ]
	then
		environment='dev'
	elif [ $hostname = 'server.promisestage' ]
	then
		environment='stage'
	elif [ $hostname = 'server.promisemaster' ]
	then
		environment='master'
	fi
fi
 
# Proceed if we know the environment
if [ ${environment:+x} ]	# Test whether variable is set and not empty
then

	# Check whether there is a gitignore file, if there isn't, create it.
	if [ ! -f .gitignore ]
	then
		cat << EOF > .gitignore
# START PROMISE AUTOCONFIG
# END PROMISE AUTOCONFIG 
EOF
	else
		# If there is no promise section, add it.
		if [ -f .count.tmp ]
		then
			rm .count.tmp
		fi
		cat .gitignore | while read line
		do
			if [ "$line" = "# START PROMISE AUTOCONFIG" ]
			then
				echo "found start" >> .count.tmp
			fi
			if [ "$line" = "# END PROMISE AUTOCONFIG" ]
			then
				echo "found end" >> .count.tmp
			fi
		done

		if [ -f .count.tmp ]
		then
			count=`wc -l < .count.tmp`
			rm .count.tmp
		else
			count=0
		fi

		if [ "$count" = "0" ]
		then
			cat << EOF >> .gitignore

# START PROMISE AUTOCONFIG
# END PROMISE AUTOCONFIG 
EOF
		elif [ "$count" = "1" ]
		then
			echo "Incomplete PROMISE AUTOCONFIG section in .gitignore"
			echo "Please ensure that BOTH of the following lines are present in the .gitignore file:"
			echo "# START PROMISE AUTOCONFIG"
			echo "# END PROMISE AUTOCONFIG"
			exit 1
		elif [ "$count" -gt "2" ]
		then
			echo "Corrupt PROMISE AUTOCONFIG section in .gitignore"
			echo "Please ensure that BOTH (and no more) of the following lines are present in the .gitignore file:"
			echo "# START PROMISE AUTOCONFIG"
			echo "# END PROMISE AUTOCONFIG"
			exit 1
		fi
	fi

	# Find the files
	find . -follow -type f -name "*.$environment" -print0 2>/dev/null | while IFS= read -r -d '' FNAME; do
		# Copy/rename the file
		cp "$FNAME" "${FNAME%.$environment}"
		# Check if file is in the index
		indexed=`git ls-files ${FNAME%.$environment}`
		if [ ${indexed:+x} ]	# Test whether variable is set and not empty
		then
			# Remove the overwritten file from the index
			git rm --cached "${FNAME%.$environment}"
		fi
		# Add the overwritten file to gitignore if it's not already there
		GITNAME=${FNAME%.$environment}
		GITNAME=${GITNAME#.}
		cat .gitignore | while read line
		do
			if [ "$line" = "# START PROMISE AUTOCONFIG" ]
			then
				found_start=true
				continue
			elif [ ! ${found_start:+x} ]	# If start NOT found
			then
				continue
			elif [ "$line" = "$GITNAME" ]
			then
				break
			elif [ "$line" = "# END PROMISE AUTOCONFIG" ]
			then
				# Add the line to the file
				sed -e '/# END PROMISE AUTOCONFIG/ i\
'$GITNAME'
' .gitignore > .gitignore.tmp
				break
			fi
		done
		if [ -f .gitignore.tmp ]
		then
			mv .gitignore.tmp .gitignore
		fi
	done

	# Find the directories
	find . -follow -type d -name "*.$environment" -print0 2>/dev/null | while IFS= read -r -d '' FNAME
	do
		# Copy/rename the directory
		if [ -d "${FNAME%.$environment}" ]
		then
			rm -rf "${FNAME%.$environment}"
		fi
		cp -R "$FNAME" "${FNAME%.$environment}"
		# Find the files inside the directory
		find "${FNAME%.$environment}"/ -follow -type f -not -name "*.$environment" -print0 2>/dev/null | while IFS= read -r -d '' SUB_FNAME; do
			# Check if file is in the index
			indexed=`git ls-files $SUB_FNAME`
			if [ ${indexed:+x} ]	# Test whether variable is set and not empty
			then
				# Remove the overwritten file from the index
				git rm --cached $SUB_FNAME 
			fi
			# Add the overwritten file to gitignore if it's not already there
			GITNAME=${SUB_FNAME%.$environment}
			GITNAME=${GITNAME#.}
			cat .gitignore | while read line
			do
				if [ "$line" = "# START PROMISE AUTOCONFIG" ]
				then
					found_start=true
					continue
				elif [ ! ${found_start:+x} ]	# If start NOT found
				then
					continue
				elif [ "$line" = "$GITNAME" ]
				then
					break
				elif [ "$line" = "# END PROMISE AUTOCONFIG" ]
				then
					# Add the line to the file
					sed -e '/# END PROMISE AUTOCONFIG/ i\
'$GITNAME'
' .gitignore > .gitignore.tmp
					break
				fi
			done
			if [ -f .gitignore.tmp ]
			then
				mv .gitignore.tmp .gitignore
			fi
		done
	done

	# If there is a file name in the .gitignore file (inside
	# the PROMISE AUTOCONFIG section) that we DON'T have a 
	# .$environment file for, remove that file name from the gitignore file
	# (because this means that the file USED to be a file we would
	# auto-copy/overwrite, but it now no longer is).

	# Loop through .gitignore
	if [ -f .lines.tmp ]
	then
		rm .lines.tmp
	fi
	cat .gitignore | while read line
	do
		if [ ! ${count:+x} ]
		then 
			count=0
		fi
		count=`expr $count + 1`
		if [ "$line" = "# START PROMISE AUTOCONFIG" ]
		then
			found_start=true
			continue
		elif [ "$line" = "# END PROMISE AUTOCONFIG" ]
		then
			break
		elif [ ! $found_start ]
		then
			continue
		fi
		# Now inside PROMISE AUTOCONFIG section.
		# Check whether the file itself or
		# any of the file's (ancestor) directories
		# have a .$environment version

		# Split the filename into an array of directories and file
		IFS='/' read -a filenames <<< "${line#/}"

		# Loop through the array elements
		testname=''
		for filename in "${filenames[@]}"
		do
			if [ ${testname:+x} ]
			then
				testname="$testname"/"$filename"
			else
				testname="$filename"
			fi
			FNAME="$testname"."$environment"
			if [ -f "$FNAME" -o -d "$FNAME" ]
			then
				# The .$environment version of the file or at least
				# one of it's ancestors exists, all is good.
				continue 2
			fi
		done
		
		# The .$environment version of the file or at least
		# one of it's ancestors doesn't exist
		# Record the line number to be removed from .gitignore.
		echo $count >> .lines.tmp

		# If the file exists, add it to the stage area
		if [ -f "${line#/}" ]
		then
			git add -f "${line#/}"	# Have to force the add because the .gitignore hasn't actually been edited yet.
		fi
	done

	# If we have lines to remove
	if [ -f .lines.tmp ]
	then
		# Prepare the collected line numbers into a sed delete expression
		# I got the following from http://stackoverflow.com/questions/1251999/sed-how-can-i-replace-a-newline-n
		sed ':a;N;$!ba;s/\n/d;/g' .lines.tmp > .lines_2.tmp
		mv .lines_2.tmp .lines.tmp
		expression="$(cat .lines.tmp)d"	# 'd' is for the final line number
		rm .lines.tmp
		sed -e "$expression" .gitignore > .gitignore.tmp
		mv .gitignore.tmp .gitignore
	fi

	# Add the .gitignore file changes to the stage
	git add .gitignore
fi
